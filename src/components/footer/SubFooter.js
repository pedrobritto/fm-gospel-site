import React from "react";
import "./SubFooter.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SubFooter = props => {
  const social = [
    { name: "Facebook", icon: "facebook", url: "#" },
    { name: "Instagram", icon: "instagram", url: "#" }
  ];

  return (
    <div className="SubFooter">
      <div className="container">
        <div className="Copyright">&copy; FM Gospel 2019</div>
        <div className="Social">
          {social.map((item, index) => {
            return (
              <a href={item.url} key={index} className="Social-item">
                <FontAwesomeIcon icon={["fab", item.icon]} />
              </a>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default SubFooter;
