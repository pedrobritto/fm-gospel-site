import React from "react";

import "./Footer.scss";

import FooterUpper from "./FooterUpper";
import SubFooter from "./SubFooter";

const Footer = () => {
  return (
    <footer className="Footer">
      <FooterUpper />

      <div className="container">
        <div className="separator Footer__separator" />
      </div>

      <SubFooter />
    </footer>
  );
};

export default Footer;
