import React from "react";
import "./FooterUpper.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import playStoreBadge from "../../assets/images/google-play-badge-redux.png";
import appStoreBadge from "../../assets/images/app-store-badge.svg";

const FooterUpper = props => {
  const patrocinio = [
    { imageUrl: "https://via.placeholder.com/70x70", alt: "Patrocinio" },
    { imageUrl: "https://via.placeholder.com/70x70", alt: "Patrocinio" },
    { imageUrl: "https://via.placeholder.com/70x70", alt: "Patrocinio" },
  ];

  return (
    <div className="FooterUpper">
      <div className="container">
        <div className="FooterUpper-col --logo">
          <div className="FooterUpper-title">Nossos Apps</div>
          <div className="AppDownload" style={{ marginTop: "2rem" }}>
            <div className="AppDownload__item">
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://play.google.com/store/apps/details?id=fmgospel.com.br&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"
              >
                <img alt="Disponível no Google Play" src={playStoreBadge} />
              </a>
            </div>
            <div className="AppDownload__item">
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://itunes.apple.com/br/app/fmgospel/id549622595?mt=8"
              >
                <img alt="Disponível na App Store" src={appStoreBadge} />
              </a>
            </div>
          </div>
        </div>

        <div className="FooterUpper-col --patrocinio">
          <div className="FooterUpper-title">Patrocínio</div>

          <div className="FooterUpper-patrocinio-list">
            {patrocinio.map((item, index) => {
              return (
                <div className="FooterUpper-patrocinio-item" key={index}>
                  <img
                    src={item.imageUrl}
                    className="FooterUpper-patrocinio-image"
                    alt={item.alt}
                  />
                </div>
              );
            })}
          </div>
        </div>

        <div className="FooterUpper-col --contato">
          <div className="FooterUpper-title">Contato</div>
          <div className="FooterUpper-contato-listing">
            <div className="FooterUpper-contato-item">
              <div className="contato-item__icon">
                <FontAwesomeIcon icon={["fab", "whatsapp"]} />
              </div>
              <a className="contato-item__content" href="https://wa.me/+55-88-99998-4077">
                (88) 99998-4077
              </a>
            </div>
            <div className="FooterUpper-contato-item">
              <div className="contato-item__icon">
                <FontAwesomeIcon icon="envelope" />
              </div>
              <a className="contato-item__content" href="mailto:contato@fmgospel.com.br">
                contato@fmgospel.com.br
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterUpper;
