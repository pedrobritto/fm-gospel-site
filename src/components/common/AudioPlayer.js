import React from "react";

const AudioPlayer = props => {
  return (
    <div className="AudioPlayer">
      <p>Ubajara</p>
      <audio src="http://8.fm5.com.br:8470/stream" controls />

      <p>Juazeiro</p>
      <audio src="http://8.fm5.com.br:8632/stream" controls />

      <p>Ibicuitinga</p>
      <audio src="http://8.fm5.com.br:8362/stream" controls />

      <p>Fortaleza</p>
      <audio src="http://11.fm5.com.br:8032/stream" controls />
    </div>
  );
};

export default AudioPlayer;
