import React from "react";
import "./Share.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Share = props => {
  const whatsappMessage = encodeURI(`Escute a FM Gospel ao vivo: https://www.fmgospel.com.br`);

  return (
    <div className="Share">
      <a
        href={`https://wa.me/?text=${whatsappMessage}`}
        target="_blank"
        rel="noopener noreferrer"
        className="Share-item whatsapp"
      >
        <div className="Share-icon">
          <FontAwesomeIcon icon={["fab", "whatsapp"]} fixedWidth />
        </div>
        <div className="Share-name">WhatsApp</div>
      </a>

      <a href="https://google.com" className="Share-item facebook">
        <div className="Share-icon">
          <FontAwesomeIcon icon={["fab", "facebook-f"]} fixedWidth />
        </div>
        <div className="Share-name">Facebook</div>
      </a>
    </div>
  );
};

export default Share;
