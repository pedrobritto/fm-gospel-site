import React from "react";

import "./ScrollBelow.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ScrollBelow = props => {
  const { href, fa } = props;
  const CustomTag = href ? "a" : "div";

  return (
    <CustomTag className="ScrollBelow" href={href ? href : ""}>
      <FontAwesomeIcon icon="chevron-down" {...fa} />
    </CustomTag>
  );
};

export default ScrollBelow;
