import React from "react";
import "./StationPicker.scss";

import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import stations from "../../stores/stations";

const StationPicker = props => {
  return (
    <div className="StationPicker">
      {stations.getState().map(item => {
        const itemClass = classNames("StationPicker-item", {
          "is-selected": item.id === props.selectedStation.id
        });

        return (
          <div
            className={itemClass}
            key={item.id}
            onClick={() =>
              props.onSelectStation({
                name: item.name,
                id: item.id,
                freq: item.freq,
                source: item.source
              })
            }
          >
            <div className="StationPicker__icon">
              <FontAwesomeIcon icon="broadcast-tower" />
            </div>

            <div className="StationPicker__text">
              <div className="StationPicker__name">{item.name}</div>
              <div className="StationPicker__freq">
                {item.freq + " "}
                {typeof item.freq === "string" ? "" : "MHz"}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default StationPicker;
