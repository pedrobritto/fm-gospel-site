import React, { Component } from "react";
import classNames from "classnames";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";
import PropTypes from "prop-types";

import "./FloatingPlayer.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import playerControls from "../../stores/playerControls";

class FloatingPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playing: false,
      volume: 1
    };

    this.player = React.createRef();

    playerControls.subscribe(() => {
      this.setState({ mute: playerControls.getState().mute });
    });
  }

  control = {
    play: () => {
      const playPromise = this.player.current.play();

      if (playPromise !== undefined) {
        playPromise
          .then(() => {
            this.player.current.play();
          })
          .catch(err => console.log(err));
      }
    },

    pause: () => {
      this.player.current.pause();
    },

    toggle: () => {
      if (this.state.playing) {
        this.control.pause();
        this.setState({ playing: false });
      } else {
        this.control.play();
        this.setState({ playing: true });
      }
    },

    volume: value => {
      const inputValue = parseInt(value);
      const volume = inputValue / 100;

      this.setState({ volume });
    },

    toggleMute: () => {
      playerControls.dispatch({ type: "TOGGLE_MUTE" });
    }
  };

  componentDidUpdate() {
    this.player.current.volume = playerControls.getState().mute ? 0 : this.state.volume;

    if (this.state.playing) {
      this.control.play();
    }
  }

  render() {
    const controlsClass = classNames("FloatingPlayer__controls", {
      "is-playing": this.state.playing
    });

    return (
      <React.Fragment>
        <div className="FloatingPlayer-box">
          <div className="FloatingPlayer">
            <audio
              className="FloatingPlayer__player"
              src={this.props.selectedStation.source}
              ref={this.player}
            />

            <div className="FloatingPlayer__container">
              <div className={controlsClass} onClick={this.control.toggle}>
                {this.state.playing ? (
                  <div className="FloatingPlayer__control FloatingPlayer__pause">
                    <FontAwesomeIcon icon="pause" />
                  </div>
                ) : (
                  <div className="FloatingPlayer__control FloatingPlayer__play">
                    <FontAwesomeIcon icon="play" />
                  </div>
                )}
              </div>

              <div className="FloatingPlayer__info">
                <div className="FloatingPlayer__station-name">
                  {this.props.selectedStation.name}
                </div>

                <div className="FloatingPlayer__station-freq">
                  {this.props.selectedStation.freq + " "}
                  {typeof this.props.selectedStation.freq === "string" ? "" : "MHz"}
                </div>
              </div>
            </div>

            <div className="FloatingPlayer__bottom-container">
              <div className="FloatingPlayer__volume-container">
                <div
                  className="FloatingPlayer__volume-icon-container"
                  onClick={this.control.toggleMute}
                >
                  {this.state.volume === 0 || playerControls.getState().mute ? (
                    <FontAwesomeIcon fixedWidth icon="volume-mute" color="#888" />
                  ) : (
                    <FontAwesomeIcon fixedWidth icon="volume-up" />
                  )}
                </div>

                <div className="FloatingPlayer__volume">
                  <Slider
                    defaultValue={100}
                    onChange={this.control.volume}
                    trackStyle={{ backgroundColor: "#124c91aa", height: 5 }}
                    handleStyle={{
                      height: 15,
                      width: 15,
                      backgroundColor: "#124c91",
                      borderColor: "transparent"
                    }}
                    railStyle={{ backgroundColor: "#e8e8e8", height: 5 }}
                  />
                </div>
              </div>
              <button
                className="FloatingPlayer__share button gradient primary"
                onClick={this.props.openShareModal}
              >
                <FontAwesomeIcon className="icon" icon="share-alt" />
                Compartilhar
              </button>

              <button
                className="FloatingPlayer__station-picker button gradient secondary"
                onClick={this.props.openStationPicker}
              >
                <FontAwesomeIcon className="icon" icon="broadcast-tower" />
                Trocar Rádio
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

FloatingPlayer.propTypes = {
  stations: PropTypes.array,
  selectedStation: PropTypes.object,
  openShareModal: PropTypes.func,
  openStationPicker: PropTypes.func
};

export default FloatingPlayer;
