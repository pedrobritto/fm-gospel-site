import React from "react";

import "./Header.scss";

import logo from "../../assets/images/logo-gospel-89.png";

const Header = () => {
  return (
    <div className="Header">
      <div className="container Header-container">
        <a href="/" className="Header-logo-container">
          <img className="Header-logo" src={logo} alt="FM Gospel" />
        </a>
      </div>
    </div>
  );
};

export default Header;
