import React from "react";
import classNames from "classnames";

import "./PageContainer.scss";

const PageContainer = props => {
  const containerClass = classNames("PageContainer", {
    "is-full": props.isFull,
  });

  return <div className={containerClass}>{props.children}</div>;
};

export default PageContainer;
