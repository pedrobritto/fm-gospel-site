import React, { Component } from "react";

import "normalize.css";
import "./helpers/fontawesome";

import PageHome from "./containers/Home";

class App extends Component {
  render() {
    return (
      <main className="App">
        <PageHome />
      </main>
    );
  }
}

export default App;
