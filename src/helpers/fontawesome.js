import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faEnvelope,
  faPlay,
  faPause,
  faCircleNotch,
  faVolumeUp,
  faVolumeMute,
  faChevronDown,
  faShareAlt,
  faTimes,
  faBroadcastTower,
  faCircle
} from "@fortawesome/free-solid-svg-icons";

import {
  faWhatsapp,
  faFacebook,
  faInstagram,
  faFacebookF
} from "@fortawesome/free-brands-svg-icons";

library.add(
  faEnvelope,
  faWhatsapp,
  faFacebook,
  faInstagram,
  faFacebookF,
  faPlay,
  faPause,
  faCircleNotch,
  faVolumeUp,
  faVolumeMute,
  faChevronDown,
  faShareAlt,
  faTimes,
  faBroadcastTower,
  faCircle
);
