import { createStore } from "redux";

import uuid from "uuid/v4";

const initialState = [
  {
    id: uuid(),
    name: "Juazeiro do Norte/CE",
    freq: 103.7,
    source: "http://8.fm5.com.br:8632/stream"
  },
  {
    id: uuid(),
    name: "Ibicuitinga/CE",
    freq: 89.3,
    source: "http://8.fm5.com.br:8362/stream"
  },
  {
    id: uuid(),
    name: "Ubajara/CE",
    freq: 93.7,
    source: "http://8.fm5.com.br:8470/stream"
  },
  {
    id: uuid(),
    name: "Fortaleza/CE",
    freq: "Rádio Online",
    source: "http://11.fm5.com.br:8032/stream"
  }
];

const reducer = state => state;

let store = createStore(reducer, initialState);

export default store;
