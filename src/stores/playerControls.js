import { createStore } from "redux";

const initialState = {
  playing: false,
  volume: 1,
  mute: false
};

const reducer = (state, action) => {
  switch (action.type) {
    case "TOGGLE_MUTE":
      return { ...state, mute: !state.mute };
    default:
      return state;
  }
};

const store = createStore(reducer, initialState);

export default store;
