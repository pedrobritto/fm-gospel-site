import React, { Component } from "react";

import Header from "../components/header/Header";
import PageContainer from "../components/layout/PageContainer";
import FloatingPlayer from "../components/common/FloatingPlayer";
import Modal from "../components/common/Modal";
import StationPicker from "../components/common/StationPicker";
import Share from "../components/common/Share";

import stations from "../stores/stations";
import Footer from "../components/footer/Footer";
import ScrollBelow from "../components/common/ScrollBelow";

class PageHome extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedStation: {},
    };

    this.stationPickerModal = React.createRef();
    this.shareModal = React.createRef();
  }

  selectFirstStation = () => {
    this.setState({
      selectedStation: {
        id: stations.getState()[0].id,
        name: stations.getState()[0].name,
        freq: stations.getState()[0].freq,
        source: stations.getState()[0].source,
      },
    });
  };

  handleSelectStation = newObj => {
    this.setState({ selectedStation: newObj });
  };

  openModal = ref => {
    ref.current.openModal();
  };

  componentDidMount() {
    this.selectFirstStation();
  }

  render() {
    return (
      <React.Fragment>
        <Header openStationPicker={() => this.openModal(this.stationPickerModal)} />

        <PageContainer isFull>
          <div className="bg-page" />
          <div className="bg-image" />
          <div className="player-title">Escute ao vivo</div>

          <FloatingPlayer
            selectedStation={this.state.selectedStation}
            openShareModal={() => this.openModal(this.shareModal)}
            openStationPicker={() => this.openModal(this.stationPickerModal)}
          />

          <ScrollBelow fa={{ color: "#fff" }} />
        </PageContainer>

        <Modal size="small" title="Escolha uma estação" ref={this.stationPickerModal}>
          <StationPicker
            selectedStation={this.state.selectedStation}
            onSelectStation={this.handleSelectStation}
          />
        </Modal>

        <Modal size="small" title="Compartilhe a FM Gospel" ref={this.shareModal}>
          <Share />
        </Modal>

        <Footer />
      </React.Fragment>
    );
  }
}

export default PageHome;
